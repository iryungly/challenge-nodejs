const express = require('express');
const app = express();
const PORT = 3000;
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.get('/', (request, response) => {
  response.render('challenge1');
});
app.get('/play', (request, response) => {
  response.render('play');
});
app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));
